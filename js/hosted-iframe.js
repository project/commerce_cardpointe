/**
 * @file
 * Defines behaviors for the CardPointe hosted iframe payment method form.
 */
(($, Drupal) => {
  Drupal.behaviors.hostedIframe = {
    attach() {
      let token = '';
      const tokenField = document.getElementById('token');
      const expirationField = document.getElementById('expiration');

      window.addEventListener('message', (event) => {
        token = JSON.parse(event.data);
        // Set the token and expiration to the hidden form fields.
        tokenField.value = token.message;
        expirationField.value = token.expiry;
      });

      $('.button--primary').click((event) => {
        // If the token is empty on form submit, delay the submission
        // so the token can generate first.
        if (token === '') {
          event.preventDefault();
          $('.delayed-input-submit').attr('disabled', 'disabled');
          setTimeout(() => {
            $('.commerce-checkout-flow-multistep-default').submit();
          }, 1000);
        }
      });
    },
  };
})(jQuery, Drupal);
