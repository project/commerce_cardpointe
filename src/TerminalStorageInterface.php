<?php

namespace Drupal\commerce_cardpointe;

use Drupal\commerce_cardpointe\Entity\TerminalInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for terminal storage.
 */
interface TerminalStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads the unchanged entity, bypassing the static cache, and locks it.
   *
   * This implements explicit, pessimistic locking as opposed to the optimistic
   * locking that will log or prevent a conflicting save. Use this method for
   * use cases that load a terminal with the explicit purpose of immediately
   * changing and saving it again. Especially if these cases may run in parallel
   * to others, for example notification/return callbacks and termination
   * events.
   *
   * @param int $terminal_id
   *   The terminal ID.
   *
   * @return \Drupal\commerce_cardpointe\Entity\TerminalInterface|null
   *   The loaded terminal or NULL if the entity cannot be loaded.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the lock could not be acquired.
   */
  public function loadForUpdate(int $terminal_id): ?TerminalInterface;

  /**
   * Loads all terminals for the given payment gateway.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway.
   *
   * @return \Drupal\commerce_cardpointe\Entity\Terminal[]
   *   The terminals.
   */
  public function loadMultipleByPaymentGateway(PaymentGatewayInterface $payment_gateway): array;

}
