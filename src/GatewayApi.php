<?php

namespace Drupal\commerce_cardpointe;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * API Cardpointe Gateway API.
 */
class GatewayApi {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new Cardpointe Gateway object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   */
  public function __construct(ClientInterface $client, LoggerInterface $logger) {
    $this->httpClient = $client;
    $this->logger = $logger;
  }

  /**
   * Submits an API request to Cardpointe.
   *
   * @param array $config
   *   The config for the client.
   * @param string $request_url
   *   The request url.
   * @param array $transaction
   *   The transaction data.
   * @param string $method
   *   HTTP method.
   *
   * @return mixed
   *   The response data.
   */
  public function apiRequest(array $config, string $request_url, array $transaction = [], string $method = 'get') {
    // Request parameters.
    $params['headers'] = [
      'Host' => $config['site'],
      'Authorization' => 'Basic ' . base64_encode($config['api_username'] . ':' . $config['api_password']),
      'Content-Type' => 'application/json',
    ];
    if (!empty($transaction)) {
      $params['json'] = $transaction;
    }

    // Log the request if specified.
    if (!empty($config['log']['request'])) {
      $this->logger->debug('CardPointe API request to @url: @param', [
        '@url' => $request_url,
        '@param' => new FormattableMarkup('<pre>' . print_r($params, 1) . '</pre>', []),
      ]);
    }

    // Submit the request to CardPointe.
    $response = $this->httpClient->request($method, $request_url, $params);
    $result = Json::decode($response->getBody()->getContents());

    // Log the response if specified.
    if (!empty($config['log']['response'])) {
      $this->logger->debug('CardPointe API server response: @param', [
        '@param' => new FormattableMarkup('<pre>' . print_r($result, 1) . '</pre>', []),
      ]);
    }

    return $result;
  }

}
