<?php

namespace Drupal\commerce_cardpointe\PluginForm\HostedIframe;

use Drupal\commerce_cardpointe\Exception\InvalidSessionKeyException;
use Drupal\commerce_cardpointe\Exception\NoRequestQueueRegisteredException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Provides the HostedIframe payment method add form.
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The terminal api.
   *
   * @var \Drupal\commerce_cardpointe\IntegratedTerminalApi
   */
  protected $terminalApi;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The main content to AJAX Response renderer.
   *
   * @var \Drupal\Core\Render\MainContent\MainContentRendererInterface
   */
  protected $ajaxRenderer;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Terminal options, keyed by HSN.
   *
   * @var string[]|null
   */
  protected ?array $terminalOptions = NULL;

  /**
   * The HSN.
   *
   * @var string|null
   */
  protected ?string $hsn = NULL;

  /**
   * The session key.
   *
   * @var string|null
   */
  protected ?string $xCardConnectSessionKey = NULL;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface|null
   */
  protected ?OrderInterface $order = NULL;

  /**
   * Whether to always disconnect after attempting an authorization.
   *
   * @var bool
   */
  protected $alwaysDisconnect;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $request_stack = $container->get('request_stack');
    if ($request_stack !== NULL) {
      $instance->request = $request_stack->getCurrentRequest();
    }
    $instance->terminalApi = $container->get('commerce_cardpointe.integrated_terminal_api');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->ajaxRenderer = $container->get('main_content_renderer.ajax');
    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    $form['payment_details'] = [
      '#parents' => array_merge($form['#parents'], ['payment_details']),
      '#type' => 'container',
      '#payment_method_type' => $payment_method->bundle(),
    ];
    if ($payment_method->bundle() === 'credit_card') {
      $form['payment_details'] = $this->buildCreditCardForm($form['payment_details'], $form_state);
    }
    elseif ($payment_method->bundle() === 'cardpointe_credit_card_terminal') {
      $form['payment_details'] = $this->buildCreditCardTerminalForm($form['payment_details'], $form_state);
    }
    // Move the billing information below the payment details.
    if (isset($form['billing_information'])) {
      $form['billing_information']['#weight'] = 10;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $element['#attached']['library'][] = 'commerce_cardpointe/hosted-iframe';
    /** @var \Drupal\commerce_cardpointe\Plugin\Commerce\PaymentGateway\HostedIframeInterface $plugin */
    $plugin = $this->plugin;
    $configuration = $plugin->getConfiguration();
    $css = self::getDefaultStyling();
    // Apply custom CSS if enabled and not empty.
    if ($configuration['iframe_styling']['enable_custom'] && !empty($configuration['custom_css'])) {
      $css = $configuration['custom_css'];
    }
    $api_url = $plugin->getApiUrl($configuration['site'], $configuration['mode']);
    $token_frame_url = Url::fromUri($api_url . '/itoke/ajax-tokenizer.html', [
      'query' => [
        'useexpiry' => 'true',
        'usecvv' => 'true',
        'formatinput' => 'true',
        'css' => $css,
      ],
    ])->toString();
    $element['#attributes']['class'][] = 'cardpointe-form';
    // Drupal 10.1.0 now supports #type iframe, but for compatibility,
    // we will continue to use html_tag.
    $element['token_frame'] = [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'src' => $token_frame_url,
        'frameborder' => '0',
        'scrolling' => 'no',
        'height' => '230px',
        'width' => '66%',
      ],
    ];
    $element['token'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => ['token'],
      ],
    ];
    $element['expiration'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => ['expiration'],
      ],
    ];
    $element['reuse_payment_method'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save payment method'),
      '#default_value' => TRUE,
      '#disabled' => ($this->routeMatch->getRouteName() === 'entity.commerce_payment_method.add_form'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // The payment gateway plugin will validate the payment details.
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    // The payment gateway plugin will process the submitted payment details.
  }

  /**
   * Returns the default CSS for iFrame styling.
   *
   * @return string
   *   The default css.
   */
  protected static function getDefaultStyling(): string {
    $default_css = '
      .error{
        color:red;
      }
      label{
        font-size:1.1em;
        font-weight: 700;
      }
      input{
        padding: 0.5rem 0.75rem;
        width: 90%;
        margin:10px 0;
      }
      select{
        padding: 0.5rem 0.75rem;
        width:20%;
        margin:10px 0;
      }';
    return $default_css;
  }

  /**
   * Get the always disconnect property.
   *
   * @return bool
   *   Whether we should always disconnect or not.
   */
  public function getAlwaysDisconnect(): bool {
    return $this->alwaysDisconnect;
  }

  /**
   * Set the always disconnect property.
   *
   * @param bool $alwaysDisconnect
   *   The always disconnect property.
   */
  public function setAlwaysDisconnect(bool $alwaysDisconnect): self {
    $this->alwaysDisconnect = $alwaysDisconnect;
    return $this;
  }

  /**
   * Get the X Card Connect Session Key.
   *
   * @return string|null
   *   The x card connect session key.
   */
  public function getCardConnectSessionKey(): ?string {
    return $this->xCardConnectSessionKey;
  }

  /**
   * Set the X Card Connect Session Key.
   *
   * @param string|null $x_card_connect_session_key
   *   The x card connect session key.
   */
  public function setCardConnectSessionKey(?string $x_card_connect_session_key): self {
    $this->xCardConnectSessionKey = $x_card_connect_session_key;
    return $this;
  }

  /**
   * Gets the credit card type from the card number.
   *
   * @param string $card_number
   *   The credit card number.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected static function getCreditCardType(string $card_number): string {
    $map = [
      'A' => 'amex',
      'D' => 'discover',
      'M' => 'mastercard',
      'V' => 'visa',
    ];
    if (!isset($map[$card_number])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_number));
    }

    return $map[$card_number];
  }

  /**
   * Get the HSN.
   *
   * @return string|null
   *   The hsn.
   */
  public function getHsn(): ?string {
    return $this->hsn;
  }

  /**
   * Set the HSN.
   *
   * @param string|null $hsn
   *   The hsn.
   */
  public function setHsn(?string $hsn): self {
    $this->hsn = $hsn;
    return $this;
  }

  /**
   * Get the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order.
   */
  public function getOrder(): ?OrderInterface {
    if ($this->order === NULL) {
      $this->order = $this->routeMatch->getParameter('commerce_order');
    }
    return $this->order;
  }

  /**
   * Set the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *   The order.
   */
  public function setOrder(?OrderInterface $order): self {
    $this->order = $order;
    return $this;
  }

  /**
   * Return the session.
   *
   * @return \Symfony\Component\HttpFoundation\Session\SessionInterface
   *   The session.
   */
  protected function getSession(): SessionInterface {
    return $this->request->getSession();
  }

  /**
   * Returns an array of terminal options.
   *
   * @return array|string[]
   *   The terminal options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTerminalOptions(): array {
    if (!$this->terminalOptions) {
      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $this->entity;
      /** @var \Drupal\commerce_cardpointe\TerminalStorageInterface $terminal_storage */
      $terminal_storage = $this->entityTypeManager->getStorage('commerce_cardpointe_terminal');
      $terminals = $terminal_storage->loadMultipleByPaymentGateway($payment_method->getPaymentGateway());
      $terminal_options = [];
      foreach ($terminals as $terminal) {
        $hsn = $terminal->getHsn();
        $terminal_options[$hsn] = $terminal->getName() . ' (' . $hsn . ')';
      }
      $this->terminalOptions = $terminal_options;
    }
    return $this->terminalOptions;
  }

  /**
   * Sets the terminal options array.
   *
   * This should be an array keyed by HSN with a string label value.
   *
   * @param string[] $terminalOptions
   *   The terminal options.
   */
  public function setTerminalOptions(array $terminalOptions): void {
    $this->terminalOptions = $terminalOptions;
  }

  /**
   * Builds the terminal credit card form.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form.
   */
  public function buildCreditCardTerminalForm(array $element, FormStateInterface $form_state): array {
    $session = $this->getSession();
    $this->setHsn($session->get('hsn'));
    $this->setCardConnectSessionKey($session->get('x_card_connect_session_key'));

    /** @var \Drupal\commerce_cardpointe\Plugin\Commerce\PaymentGateway\HostedIframeInterface $plugin */
    $plugin = $this->plugin;
    $configuration = $plugin->getConfiguration();
    $this->terminalApi->setConfiguration($configuration);

    $element['terminal'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      '#id' => 'terminal-wrapper',
    ];
    $terminal_options = [];
    if ($this->getHsn() === NULL) {
      $terminal_options = $this->getTerminalOptions();
      if (count($terminal_options) === 1) {
        $this->setHsn(key($terminal_options));
      }
    }
    if (count($terminal_options) === 0) {
      $element['terminal']['hsn'] = [
        '#markup' => $this->t('<h4>Terminal</h4><p>At least one terminal needs to be <a href="@url">enabled and configured</a>.</p>', [
          '@url' => Url::fromRoute('entity.commerce_cardpointe_terminal.collection', [
            'commerce_payment_gateway' => $this->entity->getPaymentGateway()->id(),
          ])->toString(),
        ]),
      ];
      return $element;
    }
    if ($this->getHsn() === NULL) {
      $element['terminal']['hsn'] = [
        '#type' => 'select',
        '#title' => $this->t('Terminal'),
        '#options' => $terminal_options,
        '#required' => TRUE,
      ];
    }
    else {
      $element['terminal']['hsn'] = [
        '#type' => 'item',
        '#title' => $this->t('Terminal'),
        '#markup' => $terminal_options[$this->getHsn()],
      ];
    }
    $parents = $element['#parents'];
    array_push($parents, 'terminal', 'hsn');
    $element['terminal']['auth'] = [
      '#type' => 'submit',
      '#value' => t('Authorize Card'),
      '#limit_validation_errors' => [$parents],
      '#submit' => [[$this, 'terminalAuthSubmit']],
      '#ajax' => [
        'callback' => [$this, 'terminalAjax'],
        'wrapper' => 'terminal-wrapper',
        'effect' => 'fade',
        'progress' => [
          'type' => 'bar',
          'message' => $this->t('Please have the customer complete the payment on the terminal.'),
        ],
      ],
    ];
    $element['#attached']['library'][] = 'commerce_cardpointe/terminal';
    $element['terminal']['disconnect'] = [
      '#type' => 'submit',
      '#value' => t('Disconnect'),
      '#limit_validation_errors' => [$parents],
      '#submit' => [[$this, 'terminalDisconnectSubmit']],
      '#ajax' => [
        'callback' => [$this, 'terminalAjax'],
        'wrapper' => 'terminal-wrapper',
        'effect' => 'fade',
      ],
      '#access' => $this->getCardConnectSessionKey() !== NULL,
    ];
    return $element;
  }

  /**
   * Handle a terminal connect ajax request.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function terminalAjax(array $form, FormStateInterface $form_state): AjaxResponse {
    if ($exception = $form_state->getTemporaryValue('terminal_exception')) {
      $response = new AjaxResponse();
      $response->addCommand(new MessageCommand($exception->getMessage(), NULL, ['type' => 'error'], TRUE));
      $form_state->setTemporaryValue('terminal_exception', NULL);
      return $response;
    }
    if ($redirect_url = $form_state->getTemporaryValue('terminal_redirect_url')) {
      $response = new AjaxResponse();
      $response->addCommand(new RedirectCommand($redirect_url));
      $form_state->setTemporaryValue('terminal_redirect_url', NULL);
      return $response;
    }
    $button = $form_state->getTriggeringElement();
    $array_parents = array_slice($button['#array_parents'], 0, -1);

    // Go one level up in the form, to the element container.
    $element = NestedArray::getValue($form, $array_parents);
    /** @var \Drupal\Core\Ajax\AjaxResponse $response */
    $response = $this->ajaxRenderer->renderResponse($element, $this->request, $this->routeMatch);
    if ($terminal_message = $form_state->getTemporaryValue('terminal_message')) {
      $response->addCommand(new MessageCommand($terminal_message, NULL, ['type' => 'status'], TRUE));
      $form_state->setTemporaryValue('terminal_message', NULL);
    }
    return $response;
  }

  /**
   * Handle a terminal connect submit request.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function terminalAuthSubmit(array $form, FormStateInterface $form_state): void {
    $rebuild_form = TRUE;
    $x_card_connect_session_key = NULL;
    $session = $this->getSession();
    try {
      $hsn = $this->getHsn();
      $x_card_connect_session_key = $this->getCardConnectSessionKey();
      if (($hsn !== NULL) && ($x_card_connect_session_key !== NULL)) {
        // We have a session. Ping to confirm it is valid.
        try {
          $this->terminalApi->setHsn($hsn)
            ->setCardConnectSessionKey($x_card_connect_session_key)
            ->ping();
        }
        catch (InvalidSessionKeyException | NoRequestQueueRegisteredException $exception) {
          // The session is invalid or
          // the terminal isn't registered in the queue.
          // Clear out the session key, so we can (re-)connect().
          // Note: if the terminal is not in the queue,
          // the device is asleep, doesn't have a network connection, or
          // may need the CardPointe app to be rebooted.
          $x_card_connect_session_key = NULL;
          $this->setCardConnectSessionKey(NULL);
          $this->terminalApi->setCardConnectSessionKey(NULL);
          $session->remove('x_card_connect_session_key');
        }
      }
      if (empty($x_card_connect_session_key)) {
        // We don't have an active connection. Let's create one.
        $button = $form_state->getTriggeringElement();
        if ($hsn === NULL) {
          // We don't have an HSN. Get one from the POSTED values.
          $array_parents = array_slice($button['#array_parents'], 0, -1);
          $hsn_array_parents = $array_parents;
          $hsn_array_parents[] = 'hsn';
          $hsn = $form_state->getValue($hsn_array_parents);
          $this->setHsn($hsn);
          $this->terminalApi->setHsn($hsn);
          $session->set('hsn', $hsn);
        }
        $this->terminalApi->setHsn($hsn);
        $result = $this->terminalApi->connect();
        if ($result) {
          $x_card_connect_session_key = $result->{'X-CardConnect-SessionKey'};
          $this->setCardConnectSessionKey($x_card_connect_session_key);
          $session->set('x_card_connect_session_key', $x_card_connect_session_key);
          $form_state->setTemporaryValue('terminal_message', $this->t('Connected to terminal'));
        }
      }
      if (($x_card_connect_session_key !== NULL) && ($hsn !== NULL)) {
        $this->terminalApi->setHsn($hsn)
          ->setCardConnectSessionKey($x_card_connect_session_key);
        $this->terminalApi->dateTime();
        $order = $this->order;
        $result = $this->terminalApi->authCard($order);
        if ($result) {
          // If partial payments are supported, we would need
          // to create a new price with the returned amount.
          $amount_paid = $order->getBalance();

          /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
          $payment_method = $this->entity;
          $payment_method->setReusable(FALSE);
          $payment_method->setRemoteId($result->token);
          $payment_method->set('cardpointe_terminal_card_type', self::getCreditCardType($result->binInfo->product));
          $payment_method->set('cardpointe_terminal_card_number', substr($result->token, -4));
          // Split the expiration by month and year.
          $expiration = str_split($result->expiry, 2);
          $expiration_month = $expiration[0];
          $expiration_year = '20' . $expiration[1];
          $payment_method->set('cardpointe_terminal_card_exp_month', $expiration_month);
          $payment_method->set('cardpointe_terminal_card_exp_year', $expiration_year);
          $expires = CreditCard::calculateExpirationTimestamp($expiration_month, $expiration_year);
          $payment_method->setExpiresTime($expires);
          $payment_method->save();

          $order->set('payment_gateway', $payment_method->getPaymentGateway());
          $order->set('payment_method', $payment_method);
          $order->setTotalPaid($amount_paid);
          $order->save();

          /** @var \Drupal\commerce_cardpointe\Plugin\Commerce\PaymentGateway\HostedIframeInterface $plugin */
          $plugin = $this->plugin;
          $configuration = $plugin->getConfiguration();
          $order_time = $this->time->getRequestTime();
          $capture = $configuration['intent'] === 'capture';
          $next_state = $capture ? 'completed' : 'authorization';
          $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
          $payment_gateway = $payment_method->getPaymentGateway();
          $payment_gateway_id = $payment_gateway ? $payment_gateway->id() : NULL;

          /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
          $payment = $payment_storage->create([
            'state' => 'new',
            'amount' => $amount_paid,
            'payment_gateway' => $payment_gateway_id,
            'payment_method' => $payment_method->id(),
            'order_id' => $order->id(),
          ]);
          $payment->setState($next_state);
          $payment->setAvsResponseCode($result->avsresp);
          $payment->setRemoteId($result->retref);
          if ($capture) {
            $payment->setCompletedTime($order_time);
          }
          else {
            $payment->setAuthorizedTime($order_time);
          }
          $payment->save();
          $redirect_url = Url::fromRoute('entity.commerce_payment.collection', ['commerce_order' => $order->id()])
            ->toString();
          $form_state->setTemporaryValue('terminal_redirect_url', $redirect_url);
          // On success, we don't rebuild the form, as we will be redirecting.
          $rebuild_form = FALSE;
        }
      }
    }
    catch (\Throwable $exception) {
      $this->handleException($exception, $form, $form_state);
    }
    if ($x_card_connect_session_key !== NULL && $this->alwaysDisconnect) {
      // If we have a session disconnect, if so configured.
      try {
        $this->terminalApi->disconnect();
        $this->setCardConnectSessionKey(NULL);
        $session->remove('x_card_connect_session_key');
      }
      catch (\Exception $exception) {
        $this->logger->log(RfcLogLevel::ERROR, '%type: @message in %function (line %line of %file).', Error::decodeException($exception));
      }
    }
    if ($rebuild_form) {
      // If we did not complete the transaction, we will rebuild the form.
      $form_state->setRebuild();
    }
  }

  /**
   * Handle a terminal disconnect submit request.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function terminalDisconnectSubmit(array $form, FormStateInterface $form_state): void {
    try {
      $session = $this->getSession();
      $hsn = $this->getHsn();
      $x_card_connect_session_key = $this->getCardConnectSessionKey();

      if (!empty($hsn) && !empty($x_card_connect_session_key)) {
        $this->terminalApi->setHsn($hsn);
        $this->terminalApi->setCardConnectSessionKey($x_card_connect_session_key);
        $this->terminalApi->disconnect();
      }
      $session->remove('hsn');
      $session->remove('x_card_connect_session_key');
      $this->setHsn(NULL);
      $this->setCardConnectSessionKey(NULL);
      $form_state->setTemporaryValue('terminal_message', $this->t('Disconnected from terminal'));
      $form_state->setRebuild();
    }
    catch (\Throwable $exception) {
      $this->handleException($exception, $form, $form_state);
    }
  }

  /**
   * Handle an exception during an api action.
   *
   * @param \Throwable $exception
   *   The exception.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function handleException(\Throwable $exception, array $form, FormStateInterface $form_state): void {
    // Clear out our terminal object, so we can connect.
    if (($exception instanceof InvalidSessionKeyException || $exception instanceof NoRequestQueueRegisteredException) && $this->getSession()->has('terminal')) {
      $this->getSession()->remove('terminal');
    }
    $form_state->setRebuild();
    // Set the exception as a temporary value so our ajax response code
    // can return an appropriate response.
    $form_state->setTemporaryValue('terminal_exception', $exception);
  }

}
