<?php

namespace Drupal\commerce_cardpointe\Entity;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the terminal entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_cardpointe_terminal",
 *   label = @Translation("Terminal", context = "CommerceCardpointe"),
 *   label_collection = @Translation("Terminals", context = "CommerceCardpointe"),
 *   label_singular = @Translation("terminal", context = "CommerceCardpointe"),
 *   label_plural = @Translation("terminals", context = "CommerceCardpointe"),
 *   label_count = @PluralTranslation(
 *     singular = "@count terminal",
 *     plural = "@count terminals",
 *     context = "CommerceCardpointe",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\commerce_cardpointe\TerminalStorage",
 *     "storage_schema" = "Drupal\commerce\CommerceContentEntityStorageSchema",
 *     "access" = "Drupal\commerce_cardpointe\TerminalAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_cardpointe\TerminalListBuilder",
 *     "views_data" = "Drupal\commerce\CommerceEntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\commerce_cardpointe\Form\TerminalForm",
 *       "add" = "Drupal\commerce_cardpointe\Form\TerminalForm",
 *       "edit" = "Drupal\commerce_cardpointe\Form\TerminalForm",
 *       "delete" = "Drupal\commerce_cardpointe\Form\TerminalDeleteForm",
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *   },
 *   base_table = "commerce_cardpointe_terminal",
 *   data_table = "commerce_cardpointe_terminal_field_data",
 *   admin_permission = "administer commerce_cardpointe_terminal",
 *   entity_keys = {
 *     "id" = "terminal_id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "owner" = "uid"
 *   },
 *   links = {
 *     "canonical" = "/admin/commerce/config/payment-gateways/manage/{commerce_payment_gateway}/terminals/{commerce_cardpointe_terminal}",
 *     "add-form" = "/admin/commerce/config/payment-gateways/manage/{commerce_payment_gateway}/terminals/add",
 *     "edit-form" = "/admin/commerce/config/payment-gateways/manage/{commerce_payment_gateway}/terminals/{commerce_cardpointe_terminal}/edit",
 *     "delete-form" = "/admin/commerce/config/payment-gateways/manage/{commerce_payment_gateway}/terminals/{commerce_cardpointe_terminal}/delete",
 *     "delete-multiple-form" = "/admin/commerce/config/payment-gateways/manage/{commerce_payment_gateway}/terminals/delete",
 *     "collection" = "/admin/commerce/config/payment-gateways/manage/{commerce_payment_gateway}/terminals",
 *   },
 * )
 */
class Terminal extends ContentEntityBase implements TerminalInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['commerce_payment_gateway'] = $this->getPaymentGatewayId();
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name): self {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHsn(): string {
    return $this->get('hsn')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHsn($hsn): self {
    $this->set('hsn', $hsn);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantId(): string {
    return $this->get('merchant_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMerchantId(string $merchant_id): TerminalInterface {
    $this->set('merchant_id', $merchant_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnvironment(): string {
    return $this->get('environment')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnvironment(string $environment): TerminalInterface {
    $this->set('environment', $environment);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeviceType(): string {
    return $this->get('device_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeviceType(string $device_type): TerminalInterface {
    $this->set('device_type', $device_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeviceCapabilities(): string {
    return $this->get('device_capabilities')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeviceCapabilities(string $device_capabilities): TerminalInterface {
    $this->set('device_capabilities', $device_capabilities);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $label = NULL;
    $name = $this->getName();
    if ($name) {
      $label = $this->t('Terminal @name', ['@name' => $name]);
    }
    else {
      $label = $this->t('Terminal');
    }
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?bool {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(?bool $status): self {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getData(string $key, $default = NULL) {
    $data = [];
    if (!$this->get('data')->isEmpty()) {
      $data = $this->get('data')->first()->getValue();
    }
    return $data[$key] ?? $default;
  }

  /**
   * {@inheritdoc}
   */
  public function setData(string $key, $value): self {
    $this->get('data')->__set($key, $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetData(string $key): self {
    if (!$this->get('data')->isEmpty()) {
      $data = $this->get('data')->first()->getValue();
      unset($data[$key]);
      $this->set('data', $data);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentGateway(): ?PaymentGatewayInterface {
    return $this->get('payment_gateway_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentGatewayId(): ?string {
    return $this->get('payment_gateway_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $created): self {
    $this->set('created', $created);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked(): bool {
    return (bool) $this->get('locked')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function lock(): self {
    $this->set('locked', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unlock(): self {
    $this->set('locked', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['hsn'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hsn'))
      ->setDescription(t('The terminal HSN.'))
      ->setRequired(TRUE)
      ->addConstraint('CommerceCardpointeTerminalHsnUnique')
      ->setSettings([
        'default_value' => '',
        'max_length' => 32,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The terminal name.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->addConstraint('CommerceCardpointeTerminalNameUnique')
      ->setSettings([
        'default_value' => '',
        'max_length' => 128,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['merchant_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Merchant Id'))
      ->setDescription(t('The terminal merchant id.'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 32,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['environment'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Environment'))
      ->setDescription(t('The terminal environment.'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 64,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['device_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Device type'))
      ->setDescription(t('The terminal device type.'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 128,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['device_capabilities'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Device capabilities'))
      ->setDescription(t('The terminal device capabilities.'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('Whether the terminal is enabled.'))
      ->setDefaultValue(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'on_label' => t('Enabled'),
        'off_label' => t('Disabled'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    $fields['payment_gateway_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payment gateway'))
      ->setDescription(t('The parent payment gateway.'))
      ->setSetting('target_type', 'commerce_payment_gateway')
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the terminal was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the terminal was last edited.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
      ]);

    $fields['locked'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Locked'))
      ->setSettings([
        'on_label' => t('Yes'),
        'off_label' => t('No'),
      ])
      ->setDefaultValue(FALSE);

    $fields += static::ownerBaseFieldDefinitions($entity_type);

    return $fields;
  }

}
