<?php

namespace Drupal\commerce_cardpointe\Entity;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for terminals.
 */
interface TerminalInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * The display name.
   *
   * @return string
   *   The display name.
   */
  public function getName(): string;

  /**
   * Sets the display name.
   *
   * @param string $name
   *   The display name.
   *
   * @return $this
   */
  public function setName(string $name): self;

  /**
   * The Hsn.
   *
   * @return string
   *   The Hsn.
   */
  public function getHsn(): string;

  /**
   * Sets the Hsn.
   *
   * @param string $hsn
   *   The Hsn.
   *
   * @return $this
   */
  public function setHsn(string $hsn): self;

  /**
   * The merchant id.
   *
   * @return string
   *   The merchant id.
   */
  public function getMerchantId(): string;

  /**
   * Sets the merchant id.
   *
   * @param string $merchant_id
   *   The merchant id.
   *
   * @return $this
   */
  public function setMerchantId(string $merchant_id): self;

  /**
   * The environment.
   *
   * @return string
   *   The environment.
   */
  public function getEnvironment(): string;

  /**
   * Sets the environment.
   *
   * @param string $environment
   *   The environment.
   *
   * @return $this
   */
  public function setEnvironment(string $environment): self;

  /**
   * The device type.
   *
   * @return string
   *   The device type.
   */
  public function getDeviceType(): string;

  /**
   * Sets the device type.
   *
   * @param string $device_type
   *   The device type.
   *
   * @return $this
   */
  public function setDeviceType(string $device_type): self;

  /**
   * The device capabilities.
   *
   * @return string
   *   The device capabilities.
   */
  public function getDeviceCapabilities(): string;

  /**
   * Sets the device capabilities.
   *
   * @param string $device_capabilities
   *   The device capabilities.
   *
   * @return $this
   */
  public function setDeviceCapabilities(string $device_capabilities): self;

  /**
   * Gets the terminal status.
   *
   * @return bool|null
   *   The terminal status.
   */
  public function getStatus(): ?bool;

  /**
   * Sets the terminal status.
   *
   * @param bool|null $status
   *   The status.
   *
   * @return $this
   */
  public function setStatus(?bool $status): self;

  /**
   * Gets a terminal data value with the given key.
   *
   * Used to store temporary data during terminal processing.
   *
   * @param string $key
   *   The key.
   * @param mixed $default
   *   The default value.
   *
   * @return mixed
   *   The value.
   */
  public function getData(string $key, $default = NULL);

  /**
   * Sets a terminal data value with the given key.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   *
   * @return $this
   */
  public function setData(string $key, $value): self;

  /**
   * Unsets a terminal data value with the given key.
   *
   * @param string $key
   *   The key.
   *
   * @return $this
   */
  public function unsetData(string $key): self;

  /**
   * Gets the terminal creation timestamp.
   *
   * @return int
   *   Creation timestamp of the terminal.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the terminal creation timestamp.
   *
   * @param int $created
   *   The terminal creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime(int $created): self;

  /**
   * Gets the parent payment gateway.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentGatewayInterface|null
   *   The payment gateway entity, or null.
   */
  public function getPaymentGateway(): ?PaymentGatewayInterface;

  /**
   * Gets the parent payment gateway ID.
   *
   * @return string|null
   *   The payment gateway ID, or null.
   */
  public function getPaymentGatewayId(): ?string;

  /**
   * Gets whether the order is locked.
   *
   * @return bool
   *   TRUE if the order is locked, FALSE otherwise.
   */
  public function isLocked(): bool;

  /**
   * Locks the order.
   *
   * @return $this
   */
  public function lock(): self;

  /**
   * Unlocks the order.
   *
   * @return $this
   */
  public function unlock(): self;

}
