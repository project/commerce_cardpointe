<?php

namespace Drupal\commerce_cardpointe\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\CreditCard as CreditCardHelper;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\CreditCard;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the credit card payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "cardpointe_credit_card_terminal",
 *   label = @Translation("Credit card (CardPointe Integrated Terminal)"),
 * )
 */
class TerminalCreditCard extends CreditCard {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $card_type = CreditCardHelper::getType($payment_method->get('cardpointe_terminal_card_type')->getString());
    $args = [
      '@card_type' => $card_type->getLabel(),
      '@card_number' => $payment_method->get('cardpointe_terminal_card_number')->getString(),
    ];
    return $this->t('@card_type ending in @card_number', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['cardpointe_terminal_card_type'] = BundleFieldDefinition::create('list_string')
      ->setLabel(t('Card type'))
      ->setDescription(t('The credit card type.'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values_function', [CreditCardHelper::class, 'getTypeLabels']);

    $fields['cardpointe_terminal_card_number'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Card number'))
      ->setDescription(t('The last few digits of the credit card number'))
      ->setRequired(TRUE);

    // card_exp_month and card_exp_year are not required because they might
    // not be known (tokenized non-reusable payment methods).
    $fields['cardpointe_terminal_card_exp_month'] = BundleFieldDefinition::create('integer')
      ->setLabel(t('Card expiration month'))
      ->setDescription(t('The credit card expiration month.'))
      ->setSetting('size', 'tiny');

    $fields['cardpointe_terminal_card_exp_year'] = BundleFieldDefinition::create('integer')
      ->setLabel(t('Card expiration year'))
      ->setDescription(t('The credit card expiration year.'))
      ->setSetting('size', 'small');

    return $fields;
  }

}
