<?php

namespace Drupal\commerce_cardpointe\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;

/**
 * Provides the interface for the HostedIframe payment gateway.
 */
interface HostedIframeInterface extends OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * Gets the API URL.
   *
   * @param string $site
   *   The site name.
   * @param string $mode
   *   The payment gateway mode.
   *
   * @return string
   *   The API URL.
   */
  public function getApiUrl($site, $mode);

}
