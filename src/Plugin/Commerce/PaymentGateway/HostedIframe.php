<?php

namespace Drupal\commerce_cardpointe\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the HostedIframe payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "cardpointe_hostediframe",
 *   label = "CardPointe (Hosted iFrame Tokenizer)",
 *   display_label = "CardPointe",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_cardpointe\PluginForm\HostedIframe\PaymentMethodAddForm",
 *   },
 *   modes = {
 *     "uat" = @Translation("Sandbox"),
 *     "production" = @Translation("Production"),
 *   },
 *   payment_method_types = {"credit_card", "cardpointe_credit_card_terminal"},
 *   credit_card_types = {
 *     "amex", "discover", "mastercard", "visa"
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class HostedIframe extends OnsitePaymentGatewayBase implements HostedIframeInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Cardpointe gateway used for making API calls.
   *
   * @var \Drupal\commerce_cardpointe\GatewayApi
   */
  protected $gatewayApi;

  /**
   * The integrated terminal api service.
   *
   * @var \Drupal\commerce_cardpointe\IntegratedTerminalApi
   */
  protected $terminalApi;

  /**
   * The number formatter.
   *
   * @var \Drupal\commerce_price\NumberFormatter
   */
  protected $numberFormatter;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The private temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $privateTempStore;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    $instance->gatewayApi = $container->get('commerce_cardpointe.gateway_api');
    $instance->terminalApi = $container->get('commerce_cardpointe.integrated_terminal_api');
    $instance->numberFormatter = $container->get('commerce_price.number_formatter');
    $instance->logger = $container->get('logger.channel.commerce_cardpointe');
    $instance->privateTempStore = $container->get('tempstore.private');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'site' => '',
      'merchant_id' => '',
      'api_username' => '',
      'api_password' => '',
      'iframe_styling' => [
        'enable_custom' => 0,
      ],
      'custom_css' => '',
      'intent' => 'capture',
      'log' => [
        'request' => 0,
        'response' => 0,
      ],
      'terminal' => [
        'site' => '',
        'api_key' => '',
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['payment_method_types']['credit_card']['#required'] = TRUE;
    $form['site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site'),
      '#default_value' => $this->configuration['site'],
      '#required' => TRUE,
    ];
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];
    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Username'),
      '#default_value' => $this->configuration['api_username'],
      '#required' => TRUE,
    ];
    $form['api_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Password'),
      '#default_value' => $this->configuration['api_password'],
      '#required' => TRUE,
    ];
    $form['iframe_styling'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('iFrame Styling'),
      '#options' => [
        'enable_custom' => $this->t('Define custom CSS for iFrame styling.'),
      ],
      '#default_value' => $this->configuration['iframe_styling'],
    ];
    $form['custom_css'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom CSS'),
      '#description' => $this->t(
        'Details about allowed CSS properties can be found <a href=":url" target="_blank" />here</a>
               <br />Any broken CSS properties can cause page rendering issues. <strong>Please use with caution.</strong>', [':url' => 'https://integrate.clover.com/developers/hosted-iframe-tokenizer/#allowed-css-properties']),
      '#default_value' => $this->configuration['custom_css'],
      '#rows' => 5,
      '#states' => [
        'visible' => [
          ':input[name="configuration[cardpointe_hostediframe][iframe_styling][enable_custom]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['intent'] = [
      '#type' => 'radios',
      '#title' => $this->t('Transaction type'),
      '#options' => [
        'capture' => $this->t("Capture (capture payment immediately after customer's approval)"),
        'authorize' => $this->t('Authorize (requires manual or automated capture after checkout)'),
      ],
      '#description' => $this->t('For more information on capturing a prior authorization, please refer to <a href=":url" target="_blank">Authorization and Capture</a>.', [':url' => 'https://developer.cardpointe.com/cardconnect-api#authorization-and-capture']),
      '#default_value' => $this->configuration['intent'],
    ];
    $form['terminal'] = [
      '#type' => 'fieldset',
      '#title' => t('Integrated Terminal'),
    ];
    $form['terminal']['description'] = [
      '#markup' => $this->t('Provide configuration for <a href="https://support.cardpointe.com/integrated/terminal" target="_blank">CardPointe Integrated terminal</a>.'),
    ];
    $form['terminal']['site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site'),
      '#default_value' => $this->configuration['terminal']['site'],
      '#states' => [
        'enabled' => [
          ':input[name="configuration[cardpointe_hostediframe][payment_method_types][cardpointe_credit_card_terminal]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="configuration[cardpointe_hostediframe][payment_method_types][cardpointe_credit_card_terminal]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['terminal']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $this->configuration['terminal']['api_key'],
      '#states' => [
        'enabled' => [
          ':input[name="configuration[cardpointe_hostediframe][payment_method_types][cardpointe_credit_card_terminal]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="configuration[cardpointe_hostediframe][payment_method_types][cardpointe_credit_card_terminal]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    // Add the logging configuration form elements.
    $form['log'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Log the following messages for debugging'),
      '#options' => [
        'request' => $this->t('API request messages'),
        'response' => $this->t('API response messages'),
      ],
      '#default_value' => $this->configuration['log'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);

    try {
      // Validate the merchant details to make sure the details entered
      // are correct.
      $response = $this->gatewayApi->apiRequest(
        $values,
        $this->buildRequestUrl($values['site'], $values['mode']),
        ['merchid' => $values['merchant_id']],
        'put'
      );
    }
    catch (\Exception $e) {
      $form_state->setError($form, $e->getMessage());
    }
    try {
      // Validate the terminal api details to make sure the details entered
      // are correct.
      $terminal_payment_type_enabled = !empty($values['payment_method_types']['cardpointe_credit_card_terminal']);
      if ($terminal_payment_type_enabled) {
        $this->terminalApi->setConfiguration($values);
        $this->terminalApi->listTerminals();
      }
      else {
        // If terminal payment method is not enabled, clear out the terminal
        // settings.
        $terminal_parent = $form['#parents'];
        $terminal_parent[] = 'terminal';
        $form_state->setValue($terminal_parent, [
          'site' => '',
          'api_key' => '',
        ]);
      }
    }
    catch (\Exception $e) {
      $form_state->setError($form['terminal']['api_key'], $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['site'] = $values['site'];
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['api_username'] = $values['api_username'];
      $this->configuration['api_password'] = $values['api_password'];
      $this->configuration['iframe_styling'] = $values['iframe_styling'];
      // Strip any tags from the custom CSS.
      $this->configuration['custom_css'] = strip_tags($values['custom_css']);
      $this->configuration['intent'] = $values['intent'];
      $this->configuration['log'] = $values['log'];
      $this->configuration['terminal'] = $values['terminal'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);
    $amount = $payment->getAmount();
    $capture = $this->configuration['intent'] == 'capture';

    try {
      $is_customer = $payment_method->getOwner()->id() == $this->currentUser->id();
      $is_recurring = $payment->getOrder()->bundle() == 'recurring';
      // Make sure orderid time is same as completed time so we can reuse later.
      $order_time = $this->time->getCurrentTime();
      $transaction_data = [
        'merchid' => $this->configuration['merchant_id'],
        // The orderid must be unique.
        'orderid' => $payment->getOrderId() . '-' . $order_time,
        'account' => $payment_method->getRemoteId(),
        'amount' => $this->numberFormatter->format($amount->getNumber(),
          [
            'minimum_fraction_digits' => 2,
            'use_grouping' => FALSE,
          ]
        ),
        'currency' => $amount->getCurrencyCode(),
        // Expiry is in YYYYM or YYYYMM format.
        'expiry' => $payment_method->card_exp_year->value . $payment_method->card_exp_month->value,
        // Capture the payment otherwise it will only be authorized.
        'capture' => $capture ? 'Y' : 'N',
        'ecomind' => !$is_customer && $payment_method->isReusable() && $is_recurring ? 'R' : 'E',
      ];

      if ($payment_method->isReusable()) {
        // Transaction owner: customer or merchant.
        $transaction_data['cof'] = $is_customer ? 'C' : 'M';
        $transaction_data['cofscheduled'] = !$is_customer && $is_recurring ? 'Y' : 'N';
        $transaction_data['cofpermission'] = 'Y';
      }

      // We need to add a CVV to all card-not-present transactions.
      $card_code = $this->privateTempStore->get('commerce_cardpointe')->get('card_code', '');
      if (!empty($card_code)) {
        $transaction_data['cvv2'] = $card_code;
        // Remove the card code from storage as we do not need it anymore.
        $this->privateTempStore->get('commerce_cardpointe')->delete('card_code');
      }

      // Include payment information in the request.
      $transaction_data += $this->addBillingProfile($payment_method);

      // Submit the capture/auth request to CardPointe.
      $response = $this->gatewayApi->apiRequest(
        $this->configuration,
        $this->buildRequestUrl($this->configuration['site'], $this->configuration['mode'], 'auth'),
        $transaction_data,
        'post',
      );

      if ($response['respstat'] !== 'A') {
        throw new HardDeclineException(sprintf('Could not charge the payment method. Response: %s.', $response['resptext']));
      }

      $next_state = $capture ? 'completed' : 'authorization';
      $payment->setState($next_state);
      $payment->setAvsResponseCode($response['avsresp']);
      $payment->setRemoteId($response['retref']);
      $payment->setCompletedTime($order_time);
      $payment->save();
    }
    catch (RequestException $e) {
      throw new HardDeclineException('Could not charge the payment method.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    try {
      $transaction_data = [
        'merchid' => $this->configuration['merchant_id'],
        'retref' => $payment->getRemoteId(),
        'amount' => $this->numberFormatter->format($amount->getNumber(),
          [
            'minimum_fraction_digits' => 2,
            'use_grouping' => FALSE,
          ]
        ),
      ];

      // Submit the capture request to CardPointe.
      $response = $this->gatewayApi->apiRequest(
        $this->configuration,
        $this->buildRequestUrl($this->configuration['site'], $this->configuration['mode'], 'capture'),
        $transaction_data,
        'post',
      );

      if ($response['respstat'] !== 'A') {
        throw new PaymentGatewayException(sprintf('Could not capture payment. Response: %s.', $response['resptext']));
      }

      $payment->setState('completed');
      $payment->setAmount($amount);
      $payment->save();
    }
    catch (RequestException $e) {
      throw new PaymentGatewayException(sprintf('Could not capture the payment. Message: %s.', $e->getMessage()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    try {
      $transaction_data = [
        'merchid' => $this->configuration['merchant_id'],
        'retref' => $payment->getRemoteId(),
      ];

      // Retrieves transaction information and checks whether
      // a void is possible.
      $inquire_request = 'inquire/' . $payment->getRemoteId() . '/' . $this->configuration['merchant_id'];
      $response = $this->gatewayApi->apiRequest(
        $this->configuration,
        $this->buildRequestUrl($this->configuration['site'], $this->configuration['mode'], $inquire_request),
      );

      if ($response['voidable'] !== 'Y') {
        throw new PaymentGatewayException(sprintf('This payment is not voidable yet. Status: %s.', $response['setlstat']));
      }

      // Submit the void request to CardPointe.
      $response = $this->gatewayApi->apiRequest(
        $this->configuration,
        $this->buildRequestUrl($this->configuration['site'], $this->configuration['mode'], 'void'),
        $transaction_data,
        'post',
      );

      if ($response['respstat'] !== 'A') {
        throw new PaymentGatewayException(sprintf('Payment could not be voided. Response: %s.', $response['resptext']));
      }

      $payment->setState('authorization_voided');
      $payment->save();
    }
    catch (RequestException $e) {
      throw new PaymentGatewayException(sprintf('Could not void the payment. Message: %s.', $e->getMessage()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    try {
      $transaction_data = [
        'merchid' => $this->configuration['merchant_id'],
        'retref' => $payment->getRemoteId(),
        'amount' => $this->numberFormatter->format($amount->getNumber(),
          [
            'minimum_fraction_digits' => 2,
            'use_grouping' => FALSE,
          ]
        ),
      ];

      // Retrieves transaction information and checks whether
      // a refund is possible.
      $inquire_request = 'inquire/' . $payment->getRemoteId() . '/' . $this->configuration['merchant_id'];
      $response = $this->gatewayApi->apiRequest(
        $this->configuration,
        $this->buildRequestUrl($this->configuration['site'], $this->configuration['mode'], $inquire_request),
      );

      if ($response['refundable'] !== 'Y') {
        throw new PaymentGatewayException(sprintf('This payment is not refundable yet. Status: %s.', $response['setlstat']));
      }

      // Submit the refund request to CardPointe.
      $response = $this->gatewayApi->apiRequest(
        $this->configuration,
        $this->buildRequestUrl($this->configuration['site'], $this->configuration['mode'], 'refund'),
        $transaction_data,
        'post',
      );

      if ($response['respstat'] !== 'A') {
        throw new PaymentGatewayException(sprintf('Could not refund the payment. Response: %s.', $response['resptext']));
      }

      $old_refunded_amount = $payment->getRefundedAmount();
      $new_refunded_amount = $old_refunded_amount->add($amount);
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->setState('partially_refunded');
      }
      else {
        $payment->setState('refunded');
      }
      $payment->setRefundedAmount($new_refunded_amount);
      $payment->save();
    }
    catch (RequestException $e) {
      throw new PaymentGatewayException(sprintf('Could not refund the payment. Message: %s.', $e->getMessage()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      'token', 'expiration',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new PaymentGatewayException(sprintf('In HostedIframe::createPaymentMethod(), $payment_details must contain the %s key.', $required_key));
      }
    }

    try {
      $reuse_payment_method = (bool) $payment_details['reuse_payment_method'];
      if ($reuse_payment_method) {
        $transaction_data = [
          'merchid' => $this->configuration['merchant_id'],
          'account' => $payment_details['token'],
          'amount' => 0,
          'expiry' => $payment_details['expiration'],
          'cof' => $this->currentUser->id() == $payment_method->getOwner()->id() ? 'C' : 'M',
          'ecomind' => 'E',
          'cofscheduled' => 'N',
        ];

        // Include payment information in the request.
        $transaction_data += $this->addBillingProfile($payment_method);

        // Submit the authorization request to CardPointe.
        $response = $this->gatewayApi->apiRequest(
          $this->configuration,
          $this->buildRequestUrl($this->configuration['site'], $this->configuration['mode'], 'auth'),
          $transaction_data,
          'post',
        );

        if ($response['respstat'] !== 'A') {
          throw new HardDeclineException(sprintf('Unable to verify the credit card. Response: %s.', $response['resptext']));
        }
      }

      $payment_method->setReusable($reuse_payment_method);
      // The second digit in the token is the credit card type.
      $payment_method->card_type = $this->getCreditCardType(substr($payment_details['token'], 1, 1));
      // The last four digits in the token is the card number.
      $payment_method->card_number = substr($payment_details['token'], -4);
      // Split the expiration by year and month.
      $expiration = str_split($payment_details['expiration'], 4);
      $payment_method->card_exp_year = $expiration[0];
      $payment_method->card_exp_month = $expiration[1];
      // Setting an expired time so the payment method is not reusable for now.
      $expires = CreditCard::calculateExpirationTimestamp($expiration[1], $expiration[0]);
      $payment_method->setRemoteId($payment_details['token']);
      $payment_method->setExpiresTime($expires);
      $payment_method->save();
    }
    catch (RequestException $e) {
      throw new HardDeclineException('Unable to store the credit card.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * Gets the credit card type from the card number.
   *
   * @param string $card_number
   *   The credit card number.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected function getCreditCardType(string $card_number) {
    $map = [
      3 => 'amex',
      4 => 'visa',
      5 => 'mastercard',
      6 => 'discover',
    ];
    if (!isset($map[$card_number])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_number));
    }

    return $map[$card_number];
  }

  /**
   * Gets the API URL.
   *
   * @param string $site
   *   The site name.
   * @param string $mode
   *   The payment gateway mode.
   * @param string $request
   *   Request url.
   *
   * @return string
   *   Builds the request URL.
   */
  protected function buildRequestUrl(string $site, string $mode, string $request = '') {
    return $this->getApiUrl($site, $mode) . "/cardconnect/rest/$request";
  }

  /**
   * Adds a payment profile to a transaction.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   *
   * @return array
   *   Transaction data including payment profile.
   */
  protected function addBillingProfile(PaymentMethodInterface $payment_method) {
    $transaction_data = [];

    // Include payment information in the request.
    if ($billing_profile = $payment_method->getBillingProfile()) {
      /** @var \Drupal\address\AddressInterface $address */
      $address = $billing_profile->get('address')->first();
      $transaction_data['name'] = $address->getGivenName() . ' ' . $address->getFamilyName();
      $transaction_data['company'] = $address->getOrganization();
      $transaction_data['address'] = $address->getAddressLine1();
      $transaction_data['address2'] = $address->getAddressLine2();
      $transaction_data['city'] = $address->getLocality();
      $transaction_data['region'] = $address->getAdministrativeArea();
      $transaction_data['country'] = $address->getCountryCode();
      $transaction_data['postal'] = $address->getPostalCode();
      $transaction_data['email'] = $payment_method->getOwner()->getEmail();
    }

    return $transaction_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl($site, $mode) {
    if ($mode == 'uat') {
      return "https://$site-uat.cardconnect.com";
    }
    else {
      return "https://$site.cardconnect.com";
    }
  }

}
