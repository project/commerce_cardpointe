<?php

namespace Drupal\commerce_cardpointe\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;

/**
 * Checks if a terminal hsn is unique on the site.
 *
 * @Constraint(
 *   id = "CommerceCardpointeTerminalHsnUnique",
 *   label = @Translation("Terminal HSN unique", context = "Validation")
 * )
 */
class TerminalHsnUnique extends UniqueFieldConstraint {

  /**
   * The constraint message.
   *
   * @var string
   */
  public $message = 'The terminal HSN "%value" is already in use.';

}
