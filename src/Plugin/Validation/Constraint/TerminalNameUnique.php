<?php

namespace Drupal\commerce_cardpointe\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;

/**
 * Checks if a terminal hsn is unique on the site.
 *
 * @Constraint(
 *   id = "CommerceCardpointeTerminalNameUnique",
 *   label = @Translation("Terminal name unique", context = "Validation")
 * )
 */
class TerminalNameUnique extends UniqueFieldConstraint {

  /**
   * The constraint message.
   *
   * @var string
   */
  public $message = 'The Terminal name "%value" is already in use.';

}
