<?php

namespace Drupal\commerce_cardpointe;

use Drupal\commerce_cardpointe\Exception\InvalidSessionKeyException;
use Drupal\commerce_cardpointe\Exception\NoRequestQueueRegisteredException;
use Drupal\commerce_cardpointe\Exception\NoSessionException;
use Drupal\commerce_cardpointe\Exception\OperationCancelledException;
use Drupal\commerce_cardpointe\Exception\SessionExistsException;
use Drupal\commerce_cardpointe\Exception\TerminalApiException;
use Drupal\commerce_cardpointe\Exception\TerminalCredentialsException;
use Drupal\commerce_cardpointe\Exception\TerminalInMerchantModeException;
use Drupal\commerce_cardpointe\Exception\TerminalInUseException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

/**
 * CardPointe Integrated Terminal API.
 */
class IntegratedTerminalApi {

  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The configuration.
   *
   * @var array
   */
  protected array $configuration;

  /**
   * The card connect session key.
   *
   * @var string|null
   */
  protected ?string $xCardConnectSessionKey = NULL;

  /**
   * The current hsn with which we have a session.
   *
   * @var string|null
   */
  protected ?string $hsn = NULL;

  /**
   * Constructs a new CardPointe Integrated Terminal object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   */
  public function __construct(ClientInterface $client, EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger) {
    $this->httpClient = $client;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * Submits an API request to CardPointe.
   *
   * @param string $method
   *   The HTTP method.
   * @param string $resource
   *   The resource.
   * @param array $payload
   *   The payload.
   * @param array $headers
   *   Request headers.
   *
   * @return object
   *   The response object.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   */
  protected function apiRequest(string $method, string $resource, array $payload, array $headers = []): object {
    $site = $this->getSite();
    $endpoint = $this->getEndpoint();
    $url = $endpoint . $resource;
    // Request parameters.
    $headers += [
      // @todo copied from gateway api...
      // seems this should be our actual host name.
      'Host' => $site,
      'Authorization' => $this->getApiKey(),
      'Content-Type' => 'application/json',
    ];
    $params[RequestOptions::HEADERS] = $headers;
    $params[RequestOptions::TIMEOUT] = 0;
    if (!empty($payload)) {
      $params[RequestOptions::JSON] = $payload;
    }

    // Log the request if specified.
    if ($this->getLogRequest()) {
      $this->logger->debug('CardPointe API request to @url: @param', [
        '@url' => $url,
        '@param' => new FormattableMarkup('<pre>' . print_r($params, 1) . '</pre>', []),
      ]);
    }

    try {
      // Submit the request to CardPointe.
      $response = $this->httpClient->request($method, $url, $params);
      $result_string = $response->getBody() ? $response->getBody()->getContents() : '';
      if (empty($result_string)) {
        $headers = [];
        $result_headers = $response->getHeaders();
        foreach ($result_headers as $key => $result_header) {
          if (is_array($result_header) && count($result_header) === 1) {
            $headers[$key] = reset($result_header);
          }
          else {
            $headers[$key] = $result_header;
          }
        }
        $result_string = json_encode($headers);
      }
      /** @var object $result */
      $result = json_decode($result_string, FALSE, 512, JSON_THROW_ON_ERROR);
      // Log the response if specified.
      if ($this->getLogResponse()) {
        $this->logger->debug('CardPointe API server response: @param', [
          '@param' => new FormattableMarkup('<pre>' . $result_string . '</pre>', []),
        ]);
      }
    }
    catch (\Throwable $exception) {
      $this->logger->log(RfcLogLevel::ERROR, '%type: @message in %function (line %line of %file).', Error::decodeException($exception));
      $exception_response_string = $exception->getResponse()->getBody()->getContents() ?? '{}';
      $exception_response_object = json_decode($exception_response_string, FALSE);
      $message = $exception_response_object->errorMessage ?? $exception->getMessage();
      if (preg_match('/^Session key for hsn [A-Z-0-9]+ was not valid$/', $message)) {
        throw new InvalidSessionKeyException($message, $exception->getCode(), $exception);
      }
      if (preg_match('/^No request queue registered for hsn [A-Z-0-9]+$/', $message)) {
        throw new NoRequestQueueRegisteredException($message, $exception->getCode(), $exception);
      }
      if (preg_match('/^Operation Cancelled$/', $message)) {
        throw new OperationCancelledException($message, $exception->getCode(), $exception);
      }
      if (preg_match('/^Terminal [A-Z-0-9]+ is already in use$/', $message)) {
        throw new TerminalInUseException($message, $exception->getCode(), $exception);
      }
      if (preg_match('/^hsn: [A-Z-0-9]+ is currently in merchant mode$/', $message)) {
        throw new TerminalInMerchantModeException($message, $exception->getCode(), $exception);
      }
      if ($exception->getCode() === 401) {
        throw new TerminalCredentialsException($message, $exception->getCode(), $exception);
      }
      throw new TerminalApiException($message, $exception->getCode(), $exception);
    }
    return $result;
  }

  /**
   * List the terminals.
   *
   * @return array
   *   An array of terminal HSNs.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   *
   * @see https://developer.cardpointe.com/terminal-api#listTerminals
   */
  public function listTerminals(): array {
    $payload = [
      'merchantId' => $this->getMerchantId(),
    ];
    $result = $this->apiRequest('POST', 'v1/listTerminals', $payload);
    return $result->terminals ?? [];
  }

  /**
   * Fetch terminal details.
   *
   * @return array
   *   An array of terminal object details.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   *
   * @see https://developer.cardpointe.com/terminal-api#terminalDetails
   */
  public function terminalDetails(): array {
    $payload = [
      'merchantId' => $this->getMerchantId(),
    ];
    $result = $this->apiRequest('POST', 'v3/terminalDetails', $payload);
    return $result->terminalDetails ?? [];
  }

  /**
   * Ping the terminal.
   *
   * @return object
   *   The ping response content object.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\NoSessionException
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   *
   * @see https://developer.cardpointe.com/terminal-api#ping
   */
  public function ping(): object {
    $this->verifySession();
    $payload = [
      'merchantId' => $this->getMerchantId(),
      'hsn' => $this->getHsn(),
    ];
    $headers = $this->getSessionHeaders();
    return $this->apiRequest('POST', 'v2/ping', $payload, $headers);
  }

  /**
   * Sync the date time of the terminal.
   *
   * @return object
   *   The date time response content object.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\NoSessionException
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   *
   * @see https://developer.cardpointe.com/terminal-api#dateTime
   */
  public function dateTime(): object {
    $this->verifySession();
    $now = new \DateTime();
    $payload = [
      'merchantId' => $this->getMerchantId(),
      'hsn' => $this->getHsn(),
      'dateTime' => $now->format('Y-m-d\Th:i:s'),
    ];
    $headers = $this->getSessionHeaders();
    return $this->apiRequest('POST', 'v2/dateTime', $payload, $headers);
  }

  /**
   * Connect to the terminal.
   *
   * @param bool $force
   *   Whether to force the connection.
   *   If force is set to true, any existing sessions will be destroyed and all
   *   in-flight operations on the terminal will be cancelled.
   *
   * @return object
   *   The connection response header object.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\SessionExistsException
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   *
   * @see https://developer.cardpointe.com/terminal-api#connect
   */
  public function connect(bool $force = FALSE): object {
    $this->verifyNoSession();
    $payload = [
      'merchantId' => $this->getMerchantId(),
      'hsn' => $this->getHsn(),
    ];
    if ($force) {
      $payload['force'] = $force;
    }
    $connect_result = $this->apiRequest('POST', 'v2/connect', $payload);
    // The session key likely has an expires property.
    // Grab the key only.
    if (strlen($connect_result->{'X-CardConnect-SessionKey'}) > 32) {
      $connect_result->{'X-CardConnect-SessionKey'} = substr($connect_result->{'X-CardConnect-SessionKey'}, 0, 32);
    }
    $this->setCardConnectSessionKey($connect_result->{'X-CardConnect-SessionKey'});
    return $connect_result;
  }

  /**
   * Disconnect from the terminal.
   *
   * @return object|null
   *   The disconnect response content object.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\NoSessionException
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   *
   * @see https://developer.cardpointe.com/terminal-api#disconnect
   */
  public function disconnect(): ?object {
    $this->verifySession();
    $payload = [
      'merchantId' => $this->getMerchantId(),
      'hsn' => $this->getHsn(),
    ];
    $headers = $this->getSessionHeaders();
    $result = $this->apiRequest('POST', 'v2/disconnect', $payload, $headers);
    $this->setHsn(NULL);
    $this->setCardConnectSessionKey(NULL);
    return $result;
  }

  /**
   * Attempt to cancel any in-flight actions.
   *
   * If you use the cancel command to allow a merchant or cardholder to attempt
   * to cancel a transaction in progress, you should also use the CardPointe
   * Gateway API's InquireByOrderid request to verify the status of the
   * transaction. A transaction cannot be canceled if the terminal service has
   * already initiated the authorization request to the CardPointe Gateway;
   * however, you can use the InquireByOrderid request to check the status of
   * the transaction and void if necessary.
   *
   * @return object|null
   *   The cancel response content object.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\NoSessionException
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   *
   * @see https://developer.cardpointe.com/terminal-api#cancel
   */
  public function cancel(): ?object {
    $this->verifySession();
    $payload = [
      'merchantId' => $this->getMerchantId(),
      'hsn' => $this->getHsn(),
    ];
    $headers = $this->getSessionHeaders();
    return $this->apiRequest('POST', 'v2/connect', $payload, $headers);
  }

  /**
   * Display a message on the terminal.
   *
   * @param string $message
   *   The message to display.
   *
   * @return object
   *   The display response content object.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   * @throws \Drupal\commerce_cardpointe\Exception\NoSessionException
   *
   * @see https://developer.cardpointe.com/terminal-api#display
   */
  public function display(string $message): object {
    $this->verifySession();
    $payload = [
      'merchantId' => $this->getMerchantId(),
      'hsn' => $this->getHsn(),
      'text' => $message,
    ];
    $headers = $this->getSessionHeaders();
    return $this->apiRequest('POST', 'v2/display', $payload, $headers);
  }

  /**
   * Authorize a payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   *
   * @return object
   *   The auth card response content object.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\NoSessionException
   * @throws \Drupal\commerce_cardpointe\Exception\TerminalApiException
   *
   * @see https://developer.cardpointe.com/terminal-api#authCard
   */
  public function authCard(OrderInterface $order): object {
    $this->verifySession();
    ;
    $balance_price = 0;
    if ($balance = $order->getBalance()) {
      $balance_price = $this->toMinorUnits($balance);
    }
    $payload = [
      'merchantId' => $this->getMerchantId(),
      'hsn' => $this->getHsn(),
      'amount' => $balance_price,
      'orderId' => $order->id(),
      'capture' => $this->getCapture(),
      'aid' => 'credit',
      'includePIN' => FALSE,
      'includeAVS' => FALSE,
      'includeSignature' => FALSE,
      'gzipSignature' => FALSE,
      'beep' => TRUE,
      'includeAmountDisplay' => TRUE,
      'confirmAmount' => FALSE,
      'createProfile' => FALSE,
      'printReceipt' => FALSE,
      'printExtraReceipt' => FALSE,
      'clearDisplayDelay' => 1500,
      'bin' => TRUE,
      'userFields' => [
        'Name' => $order->getCustomer()->getDisplayName() ?? '',
        'Email' => $order->getCustomer()->getEmail() ?? '',
      ],
    ];
    $headers = $this->getSessionHeaders();
    return $this->apiRequest('POST', 'v3/authCard', $payload, $headers);
  }

  /**
   * Sets the configuration from the payment gateway.
   *
   * @param array $configuration
   *   The payment gateway configuration.
   *
   * @return $this
   */
  public function setConfiguration(array $configuration): self {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * Whether we have a session.
   *
   * @return bool
   *   Whether we have a session.
   */
  protected function hasSession(): bool {
    return !empty($this->getCardConnectSessionKey());
  }

  /**
   * Verify that a session key exists.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\NoSessionException
   */
  protected function verifySession(): void {
    if ($this->getCardConnectSessionKey() === NULL) {
      throw new NoSessionException('A session key is required to invoke this action.');
    }
  }

  /**
   * Verify that a session key does not exist.
   *
   * @throws \Drupal\commerce_cardpointe\Exception\SessionExistsException
   */
  protected function verifyNoSession(): void {
    if ($this->getCardConnectSessionKey() !== NULL) {
      throw new SessionExistsException('A session already exists. Disconnect first.');
    }
  }

  /**
   * Build headers with the session key.
   *
   * @return array
   *   A headers array containing the session key.
   */
  protected function getSessionHeaders(): array {
    return [
      'X-CardConnect-SessionKey' => $this->getCardConnectSessionKey(),
    ];
  }

  /**
   * Get the configured merchant id.
   *
   * @return string|null
   *   The merchant id or NULL.
   */
  protected function getMerchantId(): ?string {
    return $this->configuration['merchant_id'] ?? NULL;
  }

  /**
   * Get the configured site.
   *
   * @return string|null
   *   The site or NULL.
   */
  protected function getSite(): ?string {
    return $this->configuration['terminal']['site'] ?? NULL;
  }

  /**
   * Whether to log the request.
   *
   * @return bool|null
   *   Log request or NULL.
   */
  protected function getLogRequest(): ?bool {
    return $this->configuration['log']['request'] ?? NULL;
  }

  /**
   * Whether to log the response.
   *
   * @return bool|null
   *   Log response or NULL.
   */
  protected function getLogResponse(): ?bool {
    return $this->configuration['log']['response'] ?? NULL;
  }

  /**
   * Get the API Key.
   *
   * @return string|null
   *   The API key.
   */
  protected function getApiKey(): ?string {
    return $this->configuration['terminal']['api_key'] ?? NULL;
  }

  /**
   * Get the HSN.
   *
   * @return string|null
   *   The HSN or null.
   */
  protected function getHsn(): ?string {
    return $this->hsn;
  }

  /**
   * Sets the HSN.
   *
   * @param string|null $hsn
   *   The HSN.
   *
   * @return $this
   */
  public function setHsn(?string $hsn): self {
    $this->hsn = $hsn;
    return $this;
  }

  /**
   * Gets the session key.
   *
   * @return string|null
   *   The session key.
   */
  public function getCardConnectSessionKey(): ?string {
    return $this->xCardConnectSessionKey;
  }

  /**
   * Sets the session key.
   *
   * @param string|null $x_card_connect_session_key
   *   The session key.
   *
   * @return $this
   */
  public function setCardConnectSessionKey(?string $x_card_connect_session_key): self {
    $this->xCardConnectSessionKey = $x_card_connect_session_key;
    return $this;
  }

  /**
   * Gets the configuration mode.
   *
   * @return string
   *   The mode.
   */
  protected function getMode(): string {
    return $this->configuration['mode'];
  }

  /**
   * Gets the endpoint.
   *
   * @return string
   *   The endpoint.
   */
  protected function getEndpoint(): string {
    $site = $this->getSite();
    $env = $this->getMode() === 'uat' ? '-uat' : '';
    return "https://$site$env.cardpointe.com/api/";
  }

  /**
   * Gets the capture intent.
   *
   * @return bool
   *   Whether to capture the authorization.
   */
  protected function getCapture(): bool {
    return $this->configuration['intent'] === 'capture';
  }

  /**
   * Returns the minor units, given the amount.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return int
   *   The minor units.
   */
  public function toMinorUnits(Price $amount): int {
    try {
      $currency_storage = $this->entityTypeManager->getStorage('commerce_currency');
      /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
      $currency = $currency_storage->load($amount->getCurrencyCode());
      $fraction_digits = $currency->getFractionDigits();
      $number = $amount->getNumber();
      if ($fraction_digits > 0) {
        $number = Calculator::multiply($number, 10 ** $fraction_digits);
      }

      return round($number, 0);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
    }
    return 0;
  }

}
