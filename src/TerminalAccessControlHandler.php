<?php

namespace Drupal\commerce_cardpointe;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity\EntityAccessControlHandler;

/**
 * Controls access based on the Terminal entity permissions.
 */
class TerminalAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\commerce_cardpointe\Entity\TerminalInterface $terminal */
    $terminal = $entity;
    /** @var \Drupal\commerce_cardpointe\Entity\TerminalInterface $entity */
    $account = $this->prepareUser($account);
    // Unlocking a terminal requires the same permissions as 'update', with
    // an additional check to ensure the terminal is actually locked.
    $additional_operation = '';
    if ($operation === 'unlock') {
      $operation = 'update';
      $additional_operation = 'unlock';
    }

    /** @var \Drupal\Core\Access\AccessResult $result */
    $result = parent::checkAccess($terminal, $operation, $account);

    if ($operation === 'view' && $result->isNeutral()) {
      if ($account->isAuthenticated() && $account->id() === $terminal->getOwnerId() && empty($additional_operation)) {
        $result = AccessResult::allowedIfHasPermissions($account, ['view own commerce_cardpointe']);
        $result = $result->cachePerUser()->addCacheableDependency($terminal);
      }
    }
    elseif (in_array($operation, ['update', 'delete'])) {
      $lock_check = ($additional_operation === 'unlock') ? $terminal->isLocked() : !$terminal->isLocked();
      $result = AccessResult::allowedIf($lock_check)->andIf($result);
      $result = $result->addCacheableDependency($terminal);
    }

    return $result;
  }

}
