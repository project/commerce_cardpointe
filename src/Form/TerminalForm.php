<?php

namespace Drupal\commerce_cardpointe\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the commerce_cardpointe entity edit forms.
 */
class TerminalForm extends ContentEntityForm {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter('commerce_cardpointe_terminal') !== NULL) {
      $entity = $route_match->getParameter('commerce_cardpointe_terminal');
    }
    else {
      /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface $commerce_payment_gateway */
      $commerce_payment_gateway = $route_match->getParameter('commerce_payment_gateway');
      $values = [
        'payment_gateway_id' => $commerce_payment_gateway,
      ];
      $entity = $this->entityTypeManager->getStorage('commerce_cardpointe_terminal')->create($values);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_cardpointe\Entity\TerminalInterface $terminal */
    $terminal = $this->entity;
    $form = parent::form($form, $form_state);

    $form['#tree'] = TRUE;
    $form['#theme'] = 'commerce_cardpointe_terminal_edit_form';
    $form['#attached']['library'][] = 'commerce_cardpointe/terminal.form';
    // Changed must be sent to the client, for later overwrite error checking.
    $form['changed'] = [
      '#type' => 'hidden',
      '#default_value' => $terminal->getChangedTime(),
    ];

    $last_saved = $this->dateFormatter->format($terminal->getChangedTime(), 'short');
    $created = $this->dateFormatter->format($terminal->getCreatedTime(), 'short');
    $form['advanced'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity-meta']],
      '#weight' => 99,
    ];
    $form['meta'] = [
      '#attributes' => ['class' => ['entity-meta__header']],
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -100,
      'date' => NULL,
      'changed' => $this->fieldAsReadOnly($this->t('Changed'), $last_saved),
      'created' => $this->fieldAsReadOnly($this->t('Created'), $created),
    ];
    $form['owner'] = [
      '#type' => 'details',
      '#title' => $this->t('Owner information'),
      '#group' => 'advanced',
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['terminal-form-author'],
      ],
      '#weight' => 91,
    ];

    // Move uid/mail widgets to the sidebar, or provide read-only alternatives.
    $owner = $terminal->getOwner();
    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'owner';
    }
    elseif (!$owner->isAnonymous()) {
      $owner_link = $owner->toLink()->toString();
      $form['owner']['uid'] = $this->fieldAsReadOnly($this->t('Owner'), $owner_link);
    }
    if (isset($form['mail'])) {
      $form['mail']['#group'] = 'owner';
    }
    elseif (!empty($terminal->getOwner()->getEmail())) {
      $form['owner']['mail'] = $this->fieldAsReadOnly($this->t('Contact email'), $terminal->getOwner()->getEmail());
    }

    return $form;
  }

  /**
   * Builds a read-only form element for a field.
   *
   * @param string $label
   *   The element label.
   * @param string $value
   *   The element value.
   *
   * @return array
   *   The form element.
   */
  protected function fieldAsReadOnly(string $label, string $value): array {
    return [
      '#type' => 'item',
      '#wrapper_attributes' => [
        'class' => ['container-inline'],
      ],
      '#title' => $label,
      '#markup' => $value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->entity->save();
    $label = $this->entity->label();
    if ($label) {
      $this->messenger()->addStatus($this->t('%label saved.', ['%label' => $this->entity->label()]));
    }
    else {
      $this->messenger()->addStatus($this->t('Terminal saved.'));
    }
    $form_state->setRedirect('entity.commerce_cardpointe_terminal.canonical', [
      'commerce_payment_gateway' => $this->entity->getPaymentGatewayId(),
      'commerce_cardpointe_terminal' => $this->entity->id(),
    ]);
  }

}
