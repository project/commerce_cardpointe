<?php

namespace Drupal\commerce_cardpointe\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting a terminal.
 */
class TerminalDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %label?', [
      '%label' => $this->getEntity()->label() ?? $this->getEntity()->id(),
    ]);
  }

}
