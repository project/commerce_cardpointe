<?php

namespace Drupal\commerce_cardpointe\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a terminal.
 */
class RefreshTerminalsForm extends FormBase {

  /**
   * The terminal api.
   *
   * @var \Drupal\commerce_cardpointe\IntegratedTerminalApi
   */
  protected $terminalApi;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The terminal storage service.
   *
   * @var \Drupal\commerce_cardpointe\TerminalStorageInterface
   */
  protected $terminalStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->terminalApi = $container->get('commerce_cardpointe.integrated_terminal_api');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->terminalStorage = $instance->entityTypeManager->getStorage('commerce_cardpointe_terminal');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'commerce_cardpointe_refresh_terminals';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['create_terminals'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new terminals'),
      '#description' => $this->t('If any terminals do not exist, this will be created.'),
      '#default_value' => TRUE,
    ];
    $form['disable_terminals'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable missing terminals'),
      '#description' => $this->t('If any existing terminals are no longer present remotely, they will be disabled.'),
      '#default_value' => TRUE,
    ];
    $form['enable_terminals'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable terminals'),
      '#description' => $this->t('If any existing terminals are disabled, they will be enabled.'),
      '#default_value' => FALSE,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $create_terminals = $values['create_terminals'];
    $disable_terminals = $values['disable_terminals'];
    $enable_terminals = $values['enable_terminals'];
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->getRouteMatch()->getParameter('commerce_payment_gateway');
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    $this->terminalApi->setConfiguration($configuration);
    $remote_terminals = $this->terminalApi->terminalDetails();
    $remote_terminals = array_column($remote_terminals, NULL, 'hsn');
    $terminals = $this->terminalStorage->loadMultipleByPaymentGateway($payment_gateway);
    foreach ($terminals as $terminal) {
      $hsn = $terminal->getHsn();
      if (!array_key_exists($hsn, $remote_terminals)) {
        if ($disable_terminals && $terminal->getStatus()) {
          $terminal->setStatus(FALSE)->save();
          $this->messenger()->addStatus($this->t('Terminal @name (@hsn) disabled.', [
            '@name' => $terminal->getName(),
            '@hsn' => $terminal->getHsn(),
          ]));
        }
      }
      else {
        if ($enable_terminals && !$terminal->getStatus()) {
          $terminal->setStatus(TRUE)->save();
          $this->messenger()->addStatus($this->t('Terminal @name (@hsn) enabled.', [
            '@name' => $terminal->getName(),
            '@hsn' => $terminal->getHsn(),
          ]));
        }
        unset($remote_terminals[$hsn]);
      }
    }
    if ($create_terminals) {
      foreach ($remote_terminals as $remote_terminal) {
        $terminal = $this->terminalStorage->create([
          'hsn' => $remote_terminal->hsn,
          'name' => $remote_terminal->hsn,
          'merchant_id' => $remote_terminal->merchantId,
          'environment' => $remote_terminal->environment,
          'device_type' => $remote_terminal->deviceType,
          'device_capabilities' => json_encode($remote_terminal->deviceCapabilities),
          'status' => TRUE,
          'payment_gateway_id' => $payment_gateway->id(),
          'uid' => $this->currentUser()->id(),
        ]);
        $terminal->save();
        $this->messenger()->addStatus($this->t('Terminal @name (@hsn) created.', [
          '@name' => $terminal->getName(),
          '@hsn' => $terminal->getHsn(),
        ]));
      }
    }
    $form_state->setRedirect('entity.commerce_cardpointe_terminal.collection', [
      'commerce_payment_gateway' => $payment_gateway->id(),
    ]);
  }

}
