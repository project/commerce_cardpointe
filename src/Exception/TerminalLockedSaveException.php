<?php

namespace Drupal\commerce_cardpointe\Exception;

/**
 * Thrown when attempting to save a terminal that is locked for updating.
 */
class TerminalLockedSaveException extends \RuntimeException {}
