<?php

namespace Drupal\commerce_cardpointe\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The invalid session key exception.
 *
 * This occurs if the session key is invalid or expired.
 */
class InvalidSessionKeyException extends TerminalApiException {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function __construct($message = "", $code = 0, ?\Throwable $previous = NULL) {
    $message = $this->t('The terminal session key is not valid. Please try the transaction again.')->render();
    parent::__construct($message, $code, $previous);
  }

}
