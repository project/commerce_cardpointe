<?php

namespace Drupal\commerce_cardpointe\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * If the terminal API credentials are not correct.
 */
class TerminalCredentialsException extends \Exception {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function __construct($message = "", $code = 0, ?\Throwable $previous = NULL) {
    $message = $this->t('The terminal API credentials are invalid. Please have an administrator validate the credentials.')->render();
    parent::__construct($message, $code, $previous);
  }

}
