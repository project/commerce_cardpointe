<?php

namespace Drupal\commerce_cardpointe\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * If the operation was cancelled at the terminal.
 */
class TerminalInUseException extends \Exception {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function __construct($message = "", $code = 0, ?\Throwable $previous = NULL) {
    $message = $this->t('The terminal is in use by another register. Please wait a moment and try again.')->render();
    parent::__construct($message, $code, $previous);
  }

}
