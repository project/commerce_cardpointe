<?php

namespace Drupal\commerce_cardpointe\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The no request queue registered exception.
 *
 * This occurs if the terminal is:
 * 1. not powered on or is asleep.
 * 2. not connected to the network.
 * 3. needs to be rebooted.
 */
class NoRequestQueueRegisteredException extends TerminalApiException {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function __construct($message = "", $code = 0, ?\Throwable $previous = NULL) {
    $message = $this->t('The terminal does not appear to be ready. Please ensure it is on, connected to the internet, and is in customer mode. If it is, then the terminal may need to be rebooted.')->render();
    parent::__construct($message, $code, $previous);
  }

}
