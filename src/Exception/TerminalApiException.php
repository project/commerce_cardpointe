<?php

namespace Drupal\commerce_cardpointe\Exception;

/**
 * General exception that occurs when calling the terminal api.
 */
class TerminalApiException extends \Exception {

}
