<?php

namespace Drupal\commerce_cardpointe\Exception;

/**
 * If we do have a session, but we shouldn't.
 */
class SessionExistsException extends \Exception {

}
