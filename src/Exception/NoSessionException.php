<?php

namespace Drupal\commerce_cardpointe\Exception;

/**
 * If we do not have a session, but we should.
 */
class NoSessionException extends \Exception {

}
