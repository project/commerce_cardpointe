<?php

namespace Drupal\commerce_cardpointe\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * If the terminal is in merchant mode.
 */
class TerminalInMerchantModeException extends \Exception {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function __construct($message = "", $code = 0, ?\Throwable $previous = NULL) {
    $message = $this->t('The terminal is in "Merchant Mode". Please put it in "Customer Mode".')->render();
    parent::__construct($message, $code, $previous);
  }

}
