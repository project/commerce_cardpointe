<?php

namespace Drupal\commerce_cardpointe\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * If the operation was cancelled at the terminal.
 */
class OperationCancelledException extends \Exception {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function __construct($message = "", $code = 0, ?\Throwable $previous = NULL) {
    $message = $this->t('The transaction was cancelled at the terminal.')->render();
    parent::__construct($message, $code, $previous);
  }

}
