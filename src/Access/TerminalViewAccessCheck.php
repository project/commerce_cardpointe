<?php

namespace Drupal\commerce_cardpointe\Access;

use Drupal\commerce_cardpointe\Plugin\Commerce\PaymentGateway\HostedIframeInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access to the terminal view.
 */
class TerminalViewAccessCheck implements AccessInterface {

  /**
   * Checks access to cardpointe terminals.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   */
  public function checkAccess(RouteMatchInterface $route_match, AccountInterface $account) {
    $payment_gateway = $route_match->getParameter('commerce_payment_gateway');
    if (!$payment_gateway instanceof PaymentGatewayInterface) {
      return AccessResult::forbidden();
    }
    $plugin = $payment_gateway->getPlugin();
    if (!$plugin instanceof HostedIframeInterface) {
      return AccessResult::forbidden();
    }
    $permissions = [
      'manage commerce_cardpointe terminals',
    ];
    $result = AccessResult::allowedIfHasPermissions($account, $permissions)->cachePerUser();
    return $result->andIf($payment_gateway->access('view', $account, TRUE));
  }

}
