<?php

namespace Drupal\commerce_cardpointe;

use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce_cardpointe\Entity\TerminalInterface;
use Drupal\commerce_cardpointe\Exception\TerminalLockedSaveException;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the terminal storage.
 */
class TerminalStorage extends CommerceContentEntityStorage implements TerminalStorageInterface {

  /**
   * List of successfully locked terminals.
   *
   * @var int[]
   */
  protected $updateLocks = [];

  /***
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lockBackend;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->lockBackend = $container->get('lock');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function invokeHook($hook, EntityInterface $entity) {
    if ($hook === 'presave') {
      // Terminal::preSave() has completed, now run the storage-level pre-save
      // tasks. These tasks can modify the terminal, so they need to run
      // before the entity/field hooks are invoked.
      $this->doTerminalPreSave($entity);
    }

    parent::invokeHook($hook, $entity);
  }

  /**
   * Performs terminal-specific pre-save tasks.
   *
   * This includes:
   * - Refreshing the terminal.
   * - Recalculating the total price.
   * - Dispatching the "terminal paid" event.
   *
   * @param \Drupal\commerce_cardpointe\Entity\TerminalInterface $terminal
   *   The terminal.
   */
  protected function doTerminalPreSave(TerminalInterface $terminal) {
    if (!$terminal->isNew() && !isset($this->updateLocks[$terminal->id()]) && !$this->lockBackend->lockMayBeAvailable($this->getLockId($terminal->id()))) {
      // This is updating a terminal that someone else has locked.
      throw new TerminalLockedSaveException('Attempted to save terminal ' . $terminal->id() . ' that is locked for updating. Use TerminalStorage::loadForUpdate().');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    try {
      return parent::save($entity);
    }
    finally {
      // Release the update lock if it was acquired for this entity.
      if (isset($this->updateLocks[$entity->id()])) {
        $this->lockBackend->release($this->getLockId($entity->id()));
        unset($this->updateLocks[$entity->id()]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadForUpdate(int $terminal_id): ?TerminalInterface {
    $lock_id = $this->getLockId($terminal_id);
    if ($this->lockBackend->acquire($lock_id)) {
      $this->updateLocks[$terminal_id] = TRUE;
      return $this->loadUnchanged($terminal_id);
    }
    else {
      // Failed to acquire initial lock, wait for it to free up.
      if (!$this->lockBackend->wait($lock_id) && $this->lockBackend->acquire($lock_id)) {
        $this->updateLocks[$terminal_id] = TRUE;
        return $this->loadUnchanged($terminal_id);
      }
      throw new EntityStorageException('Failed to acquire lock');
    }
  }

  /**
   * Gets the lock ID for the given terminal ID.
   *
   * @param int $terminal_id
   *   The terminal ID.
   *
   * @return string
   *   The lock ID.
   */
  protected function getLockId(int $terminal_id): string {
    return 'commerce_cardpointe_terminal_update:' . $terminal_id;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultipleByPaymentGateway(PaymentGatewayInterface $payment_gateway): array {
    $query = $this->getQuery()
      ->accessCheck()
      ->condition('payment_gateway_id', $payment_gateway->id())
      ->sort('terminal_id');
    $result = $query->execute();

    return $result ? $this->loadMultiple($result) : [];
  }

}
