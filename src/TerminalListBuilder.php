<?php

namespace Drupal\commerce_cardpointe;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the list builder for terminals.
 */
class TerminalListBuilder extends EntityListBuilder {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function load() {
    $commerce_payment_gateway = $this->routeMatch->getParameter('commerce_payment_gateway');
    return $this->storage->loadMultipleByPaymentGateway($commerce_payment_gateway);
  }

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'terminal_id' => [
        'data' => $this->t('Terminal ID'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'hsn' => [
        'data' => $this->t('Hsn'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'name' => [
        'data' => $this->t('Name'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'owner' => [
        'data' => $this->t('Owner'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'status' => [
        'data' => $this->t('Status'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'created' => [
        'data' => $this->t('Created'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_cardpointe\Entity\TerminalInterface $terminal */
    $terminal = $entity;
    $row = [
      'terminal_id' => $terminal->id(),
      'hsn' => $terminal->getHsn(),
      'name' => $terminal->getName(),
      'owner' => [
        'data' => [
          '#theme' => 'username',
          '#account' => $terminal->getOwner(),
        ],
      ],
      'status' => $terminal->getStatus() ? $this->t('Enabled') : $this->t('Disabled'),
      'created' => $this->dateFormatter->format($terminal->getCreatedTime(), 'short'),
    ];

    return $row + parent::buildRow($terminal);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    /** @var \Drupal\commerce_cardpointe\Entity\TerminalInterface $entity */
    if ($entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 5,
        'url' => $entity->toUrl('canonical'),
      ];
    }
    if ($entity->access('unlock')) {
      $operations['unlock'] = [
        'title' => $this->t('Unlock'),
        'weight' => 25,
        'url' => $this->ensureDestination($entity->toUrl('unlock-form')),
      ];
    }

    return $operations;
  }

}
