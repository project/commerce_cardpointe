# Commerce Cardpointe

This module allows your Commerce store to take credit card transactions using
the Cardpointe CardSecure using the hosted iframe tokenizer.

## Requirements

* Drupal 9.x and above (https://www.drupal.org/download)
* Commerce 2.x (https://www.drupal.org/project/commerce)
* Cardpointe Merchant Account (https://cardconnect.com/cardpointe)

## Setup

Use Composer to get the project
```
composer require drupal/commerce_cardpointe
```
Enable the module
```
drush en commerce_cardpointe -y
```
Create a new payment gateway at
```
admin/commerce/config/payment-gateways/add
```

## Configuration

To setup the payment gateway, you will need the following credentials from
your Cardpointe merchant account.
* Site
* Merchant ID
* API Username
* API Password
* Transaction type
* Logs

### Testing

* How to test integration https://developer.cardpointe.com/guides/cardpointe-gateway#testing-your-integration
* Gateway Test Credentials https://support.cardpointe.com/gateway-test-credentials
